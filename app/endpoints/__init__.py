from httpx import Response
from datetime import datetime
from fastapi import Path, HTTPException, APIRouter

from app.business import update_bid_status
from app.schemas import Event, EventState, Events

events: dict[str, Event] = {}
events_requiring_updating: dict[str, Event or None] = {}

router = APIRouter()


@router.put('/event', status_code=201)
async def create_event(event: Event):
    event_id = str(event.event_id)
    if event_id not in events:
        events[event_id] = event
    else:
        raise HTTPException(status_code=404, detail="Event is exists")


@router.get('/event/{event_id}', response_model=Event)
async def get_event(event_id: str = Path(default=None)):
    if event_id in events:
        return events[event_id]

    raise HTTPException(status_code=404, detail="Event not found")


@router.post('/event', status_code=200)
async def update_event(event: Event):
    event_old = events.get(str(event.event_id), None)
    if event_old is None:
        raise HTTPException(status_code=404, detail="Event not found")
    if event_old.state != EventState.NEW:
        raise HTTPException(status_code=410, detail="Event close")

    for k, v in event.dict().items():
        if v is not None:
            event_old.__setattr__(k, v)

    try:
        response: Response = await update_bid_status(event_old.event_id, event_old.state)
        if response.status_code != 200:
            raise Exception
    except Exception:
        events_requiring_updating[str(event_old.event_id)] = event_old
        raise HTTPException(status_code=500, detail='Failed to update bid status')


@router.get('/events', response_model=Events)
async def get_events():
    return Events(events=list(e for e in events.values() if datetime.utcnow() < e.deadline))

