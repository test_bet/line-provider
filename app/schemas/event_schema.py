import enum
from datetime import datetime
from typing import Optional, List
from uuid import UUID
from pydantic import BaseModel, condecimal
from decimal import Decimal


class EventState(enum.Enum):
    NEW = "NEW"
    FINISHED_WIN = "FINISHED_WIN"
    FINISHED_LOSE = "FINISHED_LOSE"


class Event(BaseModel):
    event_id: UUID
    coefficient: condecimal(gt=Decimal(0), decimal_places=2) = None
    deadline: datetime = None
    state: Optional[EventState] = None


class Events(BaseModel):
    events: List[Event]
