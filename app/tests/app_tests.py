from datetime import datetime, timedelta
from uuid import uuid4

import pytest
from httpx import AsyncClient

from app.asgi import app
from app.schemas import EventState
from app.untils import httpx_client_wrapper

base_url = 'http://localhost:8080'


@pytest.mark.parametrize('anyio_backend', ['asyncio'])
async def test_simple_workflow(anyio_backend):
    httpx_client_wrapper.start()
    test_id = str(uuid4())

    test_event = {
        'event_id': test_id,
        'coefficient': 1.0,
        'deadline': (datetime.utcnow() + timedelta(seconds=600)).strftime('%Y-%m-%dT%H:%M:%S%z'),
        'state': EventState.NEW.value
    }

    async with AsyncClient(app=app, base_url=base_url) as ac:
        create_response = await ac.put('/event', json=test_event)

    assert create_response.status_code == 201

    async with AsyncClient(app=app, base_url=base_url) as ac:
        response = await ac.get(f'/event/{test_id}')

    assert response.status_code == 200
    assert response.json() == test_event

    updated_event = test_event.copy()
    updated_event['state'] = EventState.FINISHED_WIN.value

    async with AsyncClient(app=app, base_url=base_url) as ac:
        update_response = await ac.post('/event', json={'event_id': test_id, 'state': EventState.FINISHED_WIN.value})

    assert update_response.status_code == 200

    async with AsyncClient(app=app, base_url=base_url) as ac:
        response = await ac.get(f'/event/{test_id}')

    assert response.status_code == 200
    assert response.json() == updated_event

    await httpx_client_wrapper.stop()
