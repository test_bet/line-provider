import os

URL_PREFIX = os.getenv('URL_PREFIX', '/').rstrip('/')
DEPLOY_ENV = os.getenv('DEPLOY_ENV', 'local')
BET_MAKER_URL = os.getenv('BET_MAKER_URL', 'http://bet-maker:8000/api')


def debug_mode() -> bool:
    return DEPLOY_ENV == 'local'
