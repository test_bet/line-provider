from uuid import UUID

from app.schemas import EventState
from app.settings import BET_MAKER_URL
from app.untils import httpx_client_wrapper


async def update_bid_status(event_id: UUID, state: EventState):
    async_client = httpx_client_wrapper()
    print(f"Update bid status for event: {event_id}")
    return await async_client.post(BET_MAKER_URL + '/change-status-bet',
                                   json={'event_id': str(event_id),
                                         'state': state.value})
