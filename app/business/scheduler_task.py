import random
from datetime import datetime, timedelta
from uuid import uuid4

from app.business import update_bid_status
from app.endpoints import events, events_requiring_updating
from app.schemas import Event, EventState


async def create_test_event():
    event_id = str(uuid4())
    events[event_id] = Event(event_id=event_id,
                             coefficient=round(random.uniform(00.01, 10.00), 2),
                             deadline=datetime.utcnow() + timedelta(seconds=random.randint(60, 600)),
                             state=EventState.NEW)

    print(f"create_event: {events[event_id]}")


async def update_bids_status():
    updated_bids = []
    for k, v in events_requiring_updating.items():
        response = await update_bid_status(v.event_id, v.state)
        if response.status_code == 200:
            updated_bids.append(k)

    for k in updated_bids:
        events_requiring_updating.pop(k)
