from apscheduler.schedulers.asyncio import AsyncIOScheduler
from fastapi import FastAPI
from app.business import create_test_event, update_bids_status
from app.endpoints import router
from app.untils import httpx_client_wrapper
from app.settings import debug_mode, URL_PREFIX

app = FastAPI(debug=debug_mode(),
              openapi_url='{}/openapi.json'.format(URL_PREFIX),
              docs_url='{}/docs'.format(URL_PREFIX),
              redoc_url='{}/redoc'.format(URL_PREFIX),
              )
app.include_router(router, prefix=URL_PREFIX)
scheduler = AsyncIOScheduler()


@app.on_event('startup')
async def on_startup():
    httpx_client_wrapper.start()
    await create_test_event()
    if debug_mode():
        scheduler.add_job(create_test_event, 'interval', seconds=30)
    scheduler.add_job(update_bids_status, 'interval', seconds=60)
    scheduler.start()


@app.on_event("shutdown")
async def shutdown_event():
    await httpx_client_wrapper.stop()
