FROM python:3.10

WORKDIR /srv/bet/line-provider/

COPY ./setup.py ./

RUN pip install -e ./[testing]

COPY ./app ./app

CMD ["uvicorn", "--host", "localhost", "--port", "8080", "--reload", "app.asgi:app"]